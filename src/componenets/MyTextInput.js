import React, { useEffect, useState } from 'react';
import { TextInput, Keyboard, Button, View } from 'react-native';
import Factory from '../../factory';

const MyTextInput = () => {
  const [value, onChangeText] = useState('Input request...');

  return (
    <View>
      <TextInput
        style={{
          height: 80,
          borderColor: 'gray',
          borderWidth: 1,
          padding: 5,
          margin: 5,
          borderRadius: 4,
        }}
        multiline
        clearTextOnFocus
        numberOfLines={4}
        onChangeText={(text) => onChangeText(text)}
        value={value}
      />
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-evenly',
          alignItems: 'flex-start',
          flex: 1,
          padding: 3,
        }}
      >
        <Button
          style={{
            backgroundColor: 'blue',
            flex: 1,
            padding: 20,
          }}
          title="Cancel"
          onPress={() => (
            Keyboard.dismiss(),
            console.log('canceled text is: ' + value),
            onChangeText('...')
          )}
        />
        <Button
          style={{
            backgroundColor: 'red',
            flex: 1,
            padding: 20,
          }}
          title="Submit"
          onPress={() => {
            Factory.api
              .authenticate()
              .post('/task/create', { message: value, type: 'ASKING' })
              .then(console.log)
              .catch((error) => {
                console.log('error', error.error_codes);
              });
            Keyboard.dismiss();
            onChangeText('Submitted...');
          }}
          // onPress={() => (
          //   Keyboard.dismiss(),
          //   console.log('Submitting: ' + value),
          //   Factory.api
          //     .authenticate()
          //     .post('/task/create', {
          //       message: value,
          //       type: 'ASKING',
          //     })
          //     .then(console.log)
          //     .catch(console.log),
          //   onChangeText('Submitted...')
          // )}
        />
      </View>
    </View>
  );
};

export default MyTextInput;
