import { StatusBar } from 'expo-status-bar';
import React, { useState, useEffect } from 'react';
import MapView, { Marker } from 'react-native-maps';
import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  KeyboardAvoidingView,
  Button,
  TouchableWithoutFeedback,
  Keyboard,
} from 'react-native';

import * as Location from 'expo-location';
import MyTextInput from './src/componenets/MyTextInput';
import Factory from './factory';

const App = () => {
  const [location, setLocation] = useState(null);
  const [errorMsg, setErrorMsg] = useState(null);
  const [value, onChangeText] = useState(null);

  useEffect(() => {
    (async () => {
      let { status } = await Location.requestPermissionsAsync();
      if (status !== 'granted') {
        setErrorMsg('Permission to access location was denied');
      }

      let location = await Location.getCurrentPositionAsync({});
      setLocation(location);
      //console.log(location.coords.latitude, location.coords.longitude);
      // To get user
      Factory.getUser().then((user) => {
        console.log(user);
        Factory.api.authenticate().post('/user/location', {
          location: [location.coords.latitude, location.coords.latitude],
        });
      });

      console.log('useEffect called');
    })();
  }, []);

  var testMarker = {
    id: 'testMarker',
    coordinate: {
      latitude: -122.487061265971,
      longitude: 37.7606724389241,
    },
  };
  return (
    <KeyboardAvoidingView style={{ flex: 1 }} behavior="padding">
      <View style={styles.container}>
        {location !== null ? (
          <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
            <MapView
              showsUserLocation
              style={styles.mapStyle}
              region={{
                latitude: location.coords.latitude,
                longitude: location.coords.longitude,
                latitudeDelta: 0.0922,
                longitudeDelta: 0.0421,
              }}
            />
          </TouchableWithoutFeedback>
        ) : null}
        <Marker
          coordinate={{
            latitude: -122.487061265971,
            longitude: 37.7606724389241,
          }}
        >
          <Text>Test</Text>
        </Marker>
        <View
          style={{
            position: 'absolute',
            bottom: 10,
            backgroundColor: '#fff',
            width: Dimensions.get('screen').width * 0.95,
            opacity: 0.6,
            flexDirection: 'column',
            borderRadius: 5,
          }}
        >
          <MyTextInput />
        </View>
        {/* this section is just for development purposes */}
        {location !== null ? (
          <Text style={{ position: 'absolute', top: 55 }}>
            {JSON.stringify(location.coords.latitude)}
          </Text>
        ) : null}
        {location !== null ? (
          <Text style={{ position: 'absolute', top: 40 }}>
            {JSON.stringify(location.coords.longitude)}
          </Text>
        ) : null}
        <StatusBar style="dark" />
      </View>
    </KeyboardAvoidingView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  mapStyle: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
  },
});

export default App;
