import AsyncStorage from '@react-native-community/async-storage';

AsyncStorage.getItem('AUTH_TOKEN').then(console.log);
AsyncStorage.getItem('REFRESH_TOKEN').then(console.log);
/**
 * Mayo API Helper
 * @constructor
 */
function APIHelper() {
  /** @const {boolean} */
  this.isDev = process.env.NODE_ENV === 'development';
  /** @const {boolean} */
  this.apiEndpoint = 'https://api-v2.heymayo.com';
}

/**
 * Get auth token
 * @return {!Promise<string, ?>}
 */
APIHelper.prototype.getAuthToken = function () {
  return AsyncStorage.getItem('AUTH_TOKEN_EXPIRES_AT').then(
    (authTokenExpiry) => {
      // If auth token is expired, refresh it
      if (parseInt(authTokenExpiry, 10) < Date.now()) {
        return AsyncStorage.getItem('REFRESH_TOKEN').then(
          (authRefreshToken) => {
            if (!authRefreshToken) {
              throw new Error('USER_NOT_AUTHENTICATED');
            } else {
              return this.post(
                '/auth/token/refresh',
                {},
                {
                  Authorization: `Bearer ${authRefreshToken}`,
                }
              ).then(({ authToken, refreshToken }) => {
                return AsyncStorage.setItem('AUTH_TOKEN', authToken)
                  .then(() =>
                    AsyncStorage.setItem(
                      'AUTH_TOKEN_EXPIRES_AT',
                      new Date(Date.now() + 59 * 60 * 1000).getTime().toString()
                    )
                  )
                  .then(() =>
                    AsyncStorage.setItem('REFRESH_TOKEN', refreshToken)
                  )
                  .then((authToken) => authToken);
              });
            }
          }
        );
      } else {
        return AsyncStorage.getItem('AUTH_TOKEN');
      }
    }
  );
};

/**
 * Authenticated wrapper
 * @param {string} path
 * @param {?} data
 * @param {?Object<string, string>} headers
 * @return {<!Object<string, !Promise<? ,?>>>}
 */
APIHelper.prototype.authenticate = function () {
  return {
    /**
     * @param {string} path
     * @param {?Object<string, string>} headers
     * @return {!Promise<?, ?>}
     */
    get: (path, headers) => {
      return this.getAuthToken().then((authToken) =>
        this.get(path, {
          ...headers,
          Authorization: `Bearer ${authToken}`,
        })
      );
    },
    /**
     * @param {string} path
     * @param {?} data
     * @param {?Object<string, string>} headers
     * @return {!Promise<?, ?>}
     */
    post: (path, data, headers) => {
      return this.getAuthToken().then((authToken) =>
        this.post(path, data, {
          ...headers,
          Authorization: `Bearer ${authToken}`,
        })
      );
    },
  };
};

/**
 * Get wrapper
 * @param {string} path
 * @param {?Object<string, string>} headers
 * @return {!Promise<?, ?>}
 */
APIHelper.prototype.get = function (path, headers) {
  return fetch(`${this.apiEndpoint}${path}`, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'x-is-dev': this.isDev.toString(),
      ...headers,
    },
  })
    .then((res) => {
      if (!res.ok) {
        return res
          .text()
          .then((res) => JSON.parse(res))
          .then((res) => {
            const err = new Error(res.msg);
            err.code = res.code;
            err.error_codes = res.error_codes;
            err.trace_id = res.trace_id;
            throw err;
          });
      }

      return res;
    })
    .then((res) => res.json())
    .then((res) => res.data);
};

/**
 * Post wrapper
 * @param {string} path
 * @param {?} data
 * @param {?Object<string, string>} headers
 * @return {!Promise<?, ?>}
 */
APIHelper.prototype.post = function (path, data, headers) {
  return fetch(`${this.apiEndpoint}${path}`, {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      'x-is-dev': this.isDev.toString(),
      ...headers,
    },
    body: JSON.stringify(data),
  })
    .then((res) => {
      if (!res.ok) {
        return res
          .text()
          .then((res) => JSON.parse(res))
          .then((res) => {
            const err = new Error(res.msg);
            err.code = res.code;
            err.error_codes = res.error_codes;
            err.trace_id = res.trace_id;
            throw err;
          });
      }

      return res;
    })
    .then((res) => res.json())
    .then((res) => res.data);
};

module.exports = APIHelper;
